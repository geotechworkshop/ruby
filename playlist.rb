class Song
  attr_accessor :name, :duration, :artis, :albumn
  def initialize(name, duration, artist, albumn)
    @name = name
    @duration = duration
    @artist = artist
    @artist = albumn
  end
end

class Playlist
  attr_reader :duration, :song_count
  attr_accessor :name, :songs

  def initialize(name = "Playlist 1", songs = [])
    @name = name
    @songs = songs
    @song_count = @songs.size
    calculate_duration
  end

  def add(song)
    #@songs.push(song)
    @songs << song
    @song_count += 1
    @duration += song.duration
  end

  private

  def calculate_duration
    @duration = @songs.inject(0){ |cont, song| cont + song.duration }
  end
end

song1 = Song.new("trapitos al agua", 5, "Mr Black", "Champeta")
song2 = Song.new("trapitos al agua REMIX", 5, "Mr Black", "Champeta")

songs = [song1, song2]
playlist = Playlist.new("champeta", songs)
p playlist.duration == 10
p playlist.song_count == 2

# Nice to Have
song3 = Song.new("trapitos al agua REMIX feat SAYA", 5, "Mr Black", "Champeta")
playlist.add(song3)
p playlist.duration == 15
p playlist.song_count == 4
