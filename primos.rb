def es_primo(num)
  index = 2
  while index <= num / 2
    return false if num % index == 0
    index += 1#index++
  end
  true
end

def es_primo(num)
  for index in (2..num/2)
    return false if num % index == 0
  end
  true
end

def es_primo(num)
  (2..num/2).each do |index|
    return false if num % index == 0
  end
  true
end

def es_primo(num)
  #!(2..num/2).any? { |x| num % index == 0 }
  !(2..num/2).any? do |x|
    num % index == 0
  end
end

p es_primo(1)
p es_primo(2)
p es_primo(9)
p es_primo(3)
