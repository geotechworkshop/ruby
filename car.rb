class Car
  attr_accessor :state

  STATES = %w(parking reverse driving neutral second_gear lower_gear)

  STATES.each do |state|
    define_method "#{state}?" do
      @state == state
    end
  end

  def initialize(state)
    @state = state
  end
end
