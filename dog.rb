class Dog
  #attr_reader :breed
  #attr_writer :breed
  attr_accessor :breed
  @@instances = 0

  def initialize(breed)
    @breed = breed
    @@instances += 1
  end

  def self.instances_count
    @@instances
  end
end
